#include "card.h"

Card::Card()
{
}

int Card::getISDN() const
{
    return ISDN;
}

void Card::setISDN(int value)
{
    ISDN = value;
}

Card::AreaFlag Card::getArea() const
{
    return area;
}

void Card::setArea(AreaFlag value)
{
    area = value;
}

bool Card::getFace() const
{
    return face;
}

void Card::setFace(bool value)
{
    face = value;
}

bool Card::getStand() const
{
    return stand;
}

void Card::setStand(bool value)
{
    stand = value;
}

QString Card::getName() const
{
    return name;
}

void Card::setName(const QString &value)
{
    name = value;
}

CardMoveStruct::CardMoveStruct()
{
    areaFrom = Card::No_Area;
    areaTo = Card::No_Area;
    indexFrom = -1;
    indexTo = -1;
//    reason = REASON_unknown;
}
